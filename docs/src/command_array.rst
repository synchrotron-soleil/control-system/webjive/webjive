Command Array Widget
********************

The widget enables the user to send a command with parameters to a certain device 

Widget setting 
===============

The user has the hability to select:

* A free label for the widget (string), shown before devicename/command (leave blank to hide)
* Button label (string, Submit as default value, the text of the button
* Show device name (boolean), if selected shows the name of the device following the format: device/command
* Show command name (boolean) if selected shows the name of the command following the format: device/command
* Require confirmation (boolean), shows a pop-up to the user to confirm to send the command
* Display output (boolean), displays the output coming from TANGO
* TimeOut for output display ms (int), adds a delay in showing the output and uses the same delay to hide the output
* Cooldown in seconds (integer)
* Text Color (color palette)
* Background color (color palette)
* Text size (integer)
* Font type (dropdown menu)


\ |IMG1|\ 

Widget input 
============

Based on the type accepted to the command, the widget displays the input to send the argument to the device. 
The arguments accepted to the command are divided in thee type: 

* Void
* Value
* Array

If a command doesn't accept arguments as input, means that it belongs to **Void** input type. So, the widget hides the input text. 

\ |IMG2|\ 

If a command accepts a **Value** as input, that should be an Float, String, Char and so on, the widget displays the input text where the user can insert the value to pass as argument. 
If a command accepts an **Array** as input, the widget displays the text input where insert the array. The format must be: **value1, value2, ...., valueN**, separated by commas and with **"** or *'*, based on the type. The widget convert the object in the input in a proper array, based on the input type accepted by the command, for example, it convert 0 in False if the input accepted is Boolean.

\ |IMG3|\ 

\ |IMG4|\ 

When a user clicks the button to send the command or press ENTER, if selected in the widget setting, a pop up shows the command and the input to send for confirmation. 


\ |IMG5|\ 

\*Notice: To actually send a command to the device the user must hit ENTER key, if the "Require confirmation" is
select a pop up will show to confirm the command to send


.. bottom of content

.. |IMG1| image:: _static/img/command_array_setting.png
   :height: 685 px
   :width: 295 px

.. |IMG2| image:: _static/img/void_command.png
   :height: 89 px
   :width: 502 px

.. |IMG3| image:: _static/img/array_command.png
   :height: 68 px
   :width: 913 px

.. |IMG4| image:: _static/img/input_array.png
   :height: 45 px
   :width: 464 px

.. |IMG5| image:: _static/img/array_confirmation.png
   :height: 150 px
   :width: 589 px