import { getBoundedIndexInArray, recoverLength, recoverStep } from "../../../../shared/utils/ArrayUtils";
import { getBlueHex, getGreenHex, getRedHex } from "../../../../shared/utils/ColorTool";
import { computeLowLog10, computeLowTen, computePow10 } from "../../../../shared/utils/MathTool";

const minTickDistance = 40;  // minimum distance in pixels between 2 ticks
const maxTickCount = 10; // maximum number of ticks on axis

/**
 * Computes the space between 2 ticks, knowing the step it is supposed to represent, the minimum value and the axis length.  
 * @param min The minimum value.
 * @param valueStep The step to represent.
 * @param originalStep The step in the uniformly distributed array starting to minimum and containing as many values as axis length.
 * @param length The axis length.
 */
function computeTickSpacing(min: number, valueStep: number, originalStep: number, length: number) {
  return getBoundedIndexInArray(min + valueStep, min, originalStep, length);
}

/**
 * Returns a string representation of a number, removing unnecessary digits.
 * @param value The number.
 * @param digits The expected number of digits. If NaN, the best one will be calculated.
 */
function toString(value: number, digits = NaN) {
  if (isNaN(digits)) {
    const pow10 = computeLowLog10(value);
    digits = pow10 >= 0 || pow10 === -Infinity || isNaN(pow10) ? 2 : Math.min(20, Math.abs(pow10) + 1);
  }
  return Math.abs(value - Math.round(value)) < Math.exp(-digits) ? value.toFixed(0) : value.toFixed(digits);
}

/**
 * Draws a vertical text near a tick on an axis.
 * @param text The text.
 * @param context The canvas in which to draw the text.
 * @param x The axis x position.
 * @param tickPosition The tick position (near which the text should be drawn).
 * @param textPosition  The text y position.
 * @param rotation The desired rotation (+-PI/2).
 */
function drawVerticalText(text: string, context: CanvasRenderingContext2D, x: number, tickPosition: number, textPosition: number, rotation: number) {
  context.save();
  context.moveTo(x, tickPosition);
  context.translate(textPosition, tickPosition);
  context.rotate(rotation);
  context.fillText(text, 0, 0);
  context.restore();
}

/**
 * Draws an axis in given CanvasRenderingContext2D
 * @param context The CanvasRenderingContext2D
 * @param min The minimum value 
 * @param max The maximum value
 * @param length The axis length
 * @param x The axis x position
 * @param y The axis y position
 * @param fontSize The font size to use for scale and title text
 * @param fontFamily The font family to use for scale and title  text
 * @param horizontal Whether the axis is horizontal
 * @param textOnTopOrLeft Whether the scale text should be written on top (if horizontal) or at the left (if vertical) of the axis.
 * @param ticksAcrossAxis Whether ticks can go across the axis. If false, ticks will stay at the same side as scale text.
 * @param reversed Whether the axis is reversed, i.e. from right to left if horizontal and from bottom to top if vertical.
 * @param axisTitle Optional: The axis title (empty by default).
 * @param axisColor Optional: The axis color (black by default).
 */
export function DrawAxis(context: CanvasRenderingContext2D, min: number, max: number, length: number, x: number, y: number, fontSize: number, fontFamily: string, horizontal: boolean, textOnTopOrLeft: boolean, ticksAcrossAxis: boolean, reversed: boolean, axisTitle: string = '', axisColor: number = 0) {
  if (length > 0) {
    const font = 'normal ' + fontSize + 'px ' + fontFamily;
    const scaleDelta = max - min;
    const precision = computeLowTen(scaleDelta);
    let adaptedMin = Math.floor(min / precision) * precision;
    let adaptedMax = Math.ceil(max / precision) * precision;
    if (adaptedMin === adaptedMax) {
      adaptedMin = (Math.floor(min / precision) - 1) * precision;
      adaptedMax = (Math.ceil(max / precision) + 1) * precision;
    }
    const originalStep = recoverStep(min, max, length);
    const minPosition = getBoundedIndexInArray(adaptedMin, min, originalStep, length);
    let mult = 0, tickSpacing = minTickDistance - 1, tickCount = maxTickCount + 1, valueStep = NaN;
    while (((tickCount > maxTickCount) && (tickCount > 1)) || (tickSpacing < minTickDistance)) {
      mult++;
      valueStep = mult * precision;
      tickCount = recoverLength(adaptedMin, adaptedMax, valueStep);
      tickSpacing = computeTickSpacing(min, valueStep, originalStep, length);
    }
    if (mult === 1) {
      let low10 = computePow10(computeLowLog10(scaleDelta) - 1);
      valueStep = low10;
      tickCount = recoverLength(adaptedMin, adaptedMax, valueStep);
      tickSpacing = computeTickSpacing(min, valueStep, originalStep, length);
      while (((tickCount > maxTickCount) && (tickCount > 1)) || (tickSpacing < minTickDistance)) {
        mult++;
        valueStep = mult * low10;
        tickCount = recoverLength(adaptedMin, adaptedMax, valueStep);
        tickSpacing = computeTickSpacing(min, valueStep, originalStep, length);
      }
    }
    context.beginPath();
    context.lineWidth = 1;
    context.strokeStyle = '#' + getRedHex(axisColor) + getGreenHex(axisColor) + getBlueHex(axisColor);
    x += 0.5;
    y += 0.5;
    context.moveTo(x, y);
    let tickPosition: number, textPosition: number, titlePosition: number;
    context.font = font;
    context.textAlign = 'center';
    const positionStep = reversed ? -tickSpacing : tickSpacing;
    const tickSize = 4;
    let tickStart: number, tickEnd: number;
    if (horizontal) {
      // Draw horizontal axis
      context.lineTo(x + length - 1, y);
      tickPosition = reversed ? x + (length - 1) - minPosition : x + minPosition;
      textPosition = textOnTopOrLeft ? y - (tickSize + 1) : (y + tickSize + 1);
      titlePosition = textOnTopOrLeft ? textPosition - (fontSize + 1) : textPosition + fontSize + 1;
      context.textBaseline = textOnTopOrLeft ? "bottom" : "top";
      if (axisTitle !== '') {
        context.fillText(axisTitle, x + length / 2, titlePosition);
      }
      if (textOnTopOrLeft) {
        tickStart = y - tickSize;
        tickEnd = ticksAcrossAxis ? y + tickSize : y;
      } else {
        tickStart = y + tickSize;
        tickEnd = ticksAcrossAxis ? y - tickSize : y;
      }
      for (let i = 0; i < tickCount && tickPosition >= x && tickPosition < x + length; i++) {
        context.moveTo(tickPosition, tickStart);
        context.lineTo(tickPosition, tickEnd);
        context.fillText(toString(adaptedMin + i * valueStep), tickPosition, textPosition);
        tickPosition += positionStep;
      }
    } else {
      // Draw vertical axis
      context.lineTo(x, y + length - 1);
      context.textBaseline = "bottom";
      tickPosition = reversed ? y + (length - 1) - minPosition : y + minPosition;
      textPosition = textOnTopOrLeft ? x - (tickSize + 1) : x + tickSize + 1;
      titlePosition = textOnTopOrLeft ? textPosition - fontSize : textPosition + fontSize;
      const rotation = textOnTopOrLeft ? -Math.PI / 2 : Math.PI / 2;
      if (axisTitle !== '') {
        drawVerticalText(axisTitle, context, x, y + length / 2, titlePosition, rotation);
      }
      if (textOnTopOrLeft) {
        tickStart = x - tickSize;
        tickEnd = ticksAcrossAxis ? x + tickSize : x;
      } else {
        tickStart = x + tickSize;
        tickEnd = ticksAcrossAxis ? x - tickSize : x;
      }
      for (let i = 0; i < tickCount && tickPosition >= y && tickPosition < y + length; i++) {
        context.moveTo(tickStart, tickPosition);
        context.lineTo(tickEnd, tickPosition);
        drawVerticalText(toString(adaptedMin + i * valueStep), context, x, tickPosition, textPosition, rotation);
        tickPosition += positionStep;
      }
    }
    context.stroke();
  }
}