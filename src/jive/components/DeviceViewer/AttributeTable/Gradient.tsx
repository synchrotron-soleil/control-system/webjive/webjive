import { generateNumberArray } from "../../../../shared/utils/ArrayUtils";
import { isNotColorPart } from "../../../../shared/utils/ColorTool";

// Color gradient: 5 color positions
const DEFAULT_COLOR_POS = generateNumberArray(5, 0, 1); // [0, 0.25, 0.5, 0.75, 1]
// 5 colors: dark purple, blue, green, yellow, red
const DEFAULT_RED_VALUES = [40, 0, 0, 250, 255];
const DEFAULT_GREEN_VALUES = [0, 0, 255, 250, 0];
const DEFAULT_BLUE_VALUES = [50, 255, 0, 0, 0];
// By default, we use a gradient of 65536 colors (2^16)
const DEFAULT_GRADIENT_SIZE = 65536;

export interface Gradient {
  gradientSize: number;
  rgradient: number[];
  ggradient: number[];
  bgradient: number[];
}

function checkColorPartArray(array: number[]) {
  for (let i = 0; i < array.length; i++) {
    if (isNotColorPart(array[i])) {
      throw RangeError("Color parts must be between 0 and 255");
    }
  }
}

/**
 * Ge
 * @param gradientSize 
 * @param colorPos 
 * @param rval 
 * @param gval 
 * @param bval 
 * @returns A Gradient
 */
function generateGradientNocheck(gradientSize: number, colorPos: number[], rval: number[], gval: number[], bval: number[]): Gradient {
  let rgradient: number[] = new Array(gradientSize);
  let ggradient: number[] = new Array(gradientSize);
  let bgradient: number[] = new Array(gradientSize);
  let colId = 0;
  for (let i = 0; i < gradientSize; i++) {
    let r = i / gradientSize;
    if (colId < colorPos.length - 2 && r >= colorPos[colId + 1]) {
      colId++;
    }
    let r1 = rval[colId], r2 = rval[colId + 1];
    let g1 = gval[colId], g2 = gval[colId + 1];
    let b1 = bval[colId], b2 = bval[colId + 1];
    let proportion = (r - colorPos[colId]) / (colorPos[colId + 1] - colorPos[colId]);
    if (proportion < 0.0) {
      proportion = 0.0;
    }
    if (proportion > 1.0) {
      proportion = 1.0;
    }
    rgradient[i] = (r1 + (r2 - r1) * proportion);
    ggradient[i] = (g1 + (g2 - g1) * proportion);
    bgradient[i] = (b1 + (b2 - b1) * proportion);
  }
  return { gradientSize, rgradient, ggradient, bgradient } as Gradient;
}

/**
 * Generates a new Gradient of given size, with given colors at given positions
 * @param gradientSize The desired gradient size.
 * @param colorPos The desired color positions.
 * @param rval The desired colors red part.
 * @param gval The desired colors green part.
 * @param bval The desired colors blue part.
 * @returns A Gradient.
 * @throws {RangeError} if arrays don't have the same length or if the values in rval, gval or bval are not valid color parts (a valid color part is 0 <= value <= 255).
 */
export function generateGradient(gradientSize: number = DEFAULT_GRADIENT_SIZE, colorPos: number[] = DEFAULT_COLOR_POS, rval: number[] = DEFAULT_RED_VALUES, gval: number[] = DEFAULT_GREEN_VALUES, bval: number[] = DEFAULT_BLUE_VALUES): Gradient {
  let gradient: Gradient;
  if (colorPos === DEFAULT_COLOR_POS && rval === DEFAULT_RED_VALUES && gval === DEFAULT_GREEN_VALUES && bval === DEFAULT_RED_VALUES) {
    gradient = generateGradientNocheck(gradientSize, colorPos, rval, gval, bval);
  } else {
    if ((colorPos.length !== rval.length) || (colorPos.length !== gval.length) || (colorPos.length !== bval.length)) {
      throw RangeError('colorPos and color arrays must be of same length');
    }
    checkColorPartArray(rval);
    checkColorPartArray(gval);
    checkColorPartArray(bval);
    gradient = generateGradientNocheck(gradientSize, colorPos, rval, gval, bval);
  }
  return gradient;
}