/**
 * Listen to mouse position.
 * Code based on the solution of Jason BROWN:
 * https://codedaily.io/tutorials/60/Create-a-useMousePosition-Hook-with-useEffect-and-useState-in-React
 */
import { useEffect, useState } from "react";

export interface MousePosition {
  x: number;
  y: number;
}

export const UseMousePosition = () => {
  const [position, setPosition] = useState({ x: 0, y: 0 } as MousePosition);

  useEffect(() => {
    const setFromEvent = (e: MouseEvent) => setPosition({ x: e.clientX, y: e.clientY } as MousePosition);
    const clearPosition = () => setPosition({ x: NaN, y:NaN } as MousePosition);
    window.addEventListener("mousemove", setFromEvent);
    window.addEventListener("mouseover", setFromEvent);
    window.addEventListener("mouseout", clearPosition);

    return () => {
      window.removeEventListener("mousemove", setFromEvent);
      window.removeEventListener("mouseover", setFromEvent);
      window.removeEventListener("mouseout", clearPosition);
    };
  }, []);

  return position;
};