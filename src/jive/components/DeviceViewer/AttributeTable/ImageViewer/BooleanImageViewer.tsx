import { BooleanImage } from "./ImageType";

/**
 * Displays a boolean image.
 * @param image The image to display.
 * @param context The canvas in which to draw the image representation.
 * @param width The image width.
 * @param height The image height.
 */
 export default function DisplayBooleanImage(image: BooleanImage, context: CanvasRenderingContext2D, width: number, height: number, imageOffsetX: number, imageOffsetY: number, gradientWidth: number, gradientOffset: number, axisFontSize: number, axisFontFamily: string) {
  const imageData = context.createImageData(width, height);
  let index = 0;
  for (const row of image) {
    for (const value of row) {
      let rgb = value ? 255 : 100, alpha = 255;
      imageData.data[index++] = rgb; // red
      imageData.data[index++] = rgb; // green
      imageData.data[index++] = rgb; // blue
      imageData.data[index++] = alpha; // alpha
    }
  }
  context.putImageData(imageData, imageOffsetX, imageOffsetY);

  // Draw legend
  const squareSize = gradientWidth / 2;
  const x = imageOffsetX + width + gradientOffset + 0.5;

  let y: number;
  context.font = 'normal ' + axisFontSize + 'px ' + axisFontFamily;
  context.textAlign = 'start';
  context.textBaseline = 'middle';
  context.lineWidth = 1;

  context.beginPath();

  y = imageOffsetY + 0.5;
  context.fillStyle = '#ffffff';
  context.fillRect(x + 0.5, y + 0.5, squareSize - 1, squareSize - 1);
  context.fillStyle = '#000000';
  context.strokeStyle = '#000000'
  context.rect(x, y, squareSize, squareSize);
  context.fillText('true', x + squareSize + 2, y + squareSize / 2);

  y = imageOffsetY + gradientWidth + squareSize + 0.5;
  context.fillStyle = '#646464';
  context.fillRect(x + 0.5, y + 0.5, squareSize - 1, squareSize - 1);
  context.fillStyle = '#000000';
  context.strokeStyle = '#000000'
  context.rect(x, y, squareSize, squareSize);
  context.fillText('false', x + squareSize + 2, y + squareSize / 2);

  context.stroke();
}
