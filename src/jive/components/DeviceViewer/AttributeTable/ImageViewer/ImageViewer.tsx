import React, { useEffect, useRef, useState } from "react";
import TangoAPI from "../../../../../shared/api/tangoAPI";
import Modal from "../../../../../shared/modal/components/Modal/Modal";
import { parseFormat } from "../../../../../shared/utils/NumberFormat";
import { DrawAxis } from "../Axis";
import { MousePosition, UseMousePosition } from "../UseMousePosition";
import DisplayBooleanImage from "./BooleanImageViewer";
import updateImageInfo from "./ImageInfo";
import { AttributeInfo, BooleanImage, DisplayableImage, NumberImage, StringImage } from "./ImageType";
import DisplayNumberImage from "./NumberImageViewer";
import DisplayStringImage from "./StringImageViewer";

const infoDiv = "imageInfo";
const imageCanvas = "imageCanvas";

// display offsets
const axisFontSize = 12;
const imageOffsetX = 2 * (axisFontSize + 1) + 5;
const imageOffsetY = imageOffsetX;
const gradientOffset = imageOffsetX + 1;
const gradientWidth = 20;

const axisFontFamily = 'sans-serif';

var mousePosition: MousePosition;

// image information
var lastImage: DisplayableImage | undefined;

/**
 * Displays an image.
 * @param displayableImage The image to display.
 * @param format The number format to use (example: '%4.3f')
 */
function DisplayImage(displayableImage: DisplayableImage) {
  const image = displayableImage.value;
  const height = (image === undefined) || (image === null) ? 0 : image.length;
  const width = (height === 0) || (image[0] === undefined) || (image[0] === null) ? 0 : image[0].length;
  let result: JSX.Element;
  const canvasRef = useRef<HTMLCanvasElement>(null);
  if ((height === 0) || (width === 0) || (typeof image[0][0] === "string")) {
    lastImage = undefined;
    result = DisplayStringImage(image as StringImage, width, height);
  } else {
    lastImage = displayableImage;
    const content = <canvas ref={canvasRef} id={imageCanvas} />;
    const contentDiv = <div style={{ display: "block", width: "100%", height: "400px", overflow: "scroll" }}>
      {content}
      <div id={infoDiv}> </div>
    </div>
    result = contentDiv;
    const canvas = canvasRef.current;
    if (canvas != null) {
      const context = canvas.getContext("2d");
      if (context != null) {
        canvas.width = width + 2 * imageOffsetX + gradientOffset + gradientWidth;
        canvas.height = height + 2 * imageOffsetY;
        if (typeof image[0][0] === "number") {
          DisplayNumberImage(image as NumberImage, context, width, height, imageOffsetX, imageOffsetY, gradientWidth, gradientOffset, axisFontSize, axisFontFamily);
        } else {
          DisplayBooleanImage(image as BooleanImage, context, width, height, imageOffsetX, imageOffsetY, gradientWidth, gradientOffset, axisFontSize, axisFontFamily);
        }
        DrawAxis(context, 0, width - 1, width, imageOffsetX, imageOffsetY - 1, axisFontSize, axisFontFamily, true, true, false, false);
        DrawAxis(context, 0, height - 1, height, imageOffsetX - 1, imageOffsetY, axisFontSize, axisFontFamily, false, true, false, false);
      }
    } // end if (canvas != null)
  } // end if ((height === 0) || (width === 0) || (typeof image[0][0] === "string")) ... else
  updateImageInfo(infoDiv, imageCanvas, mousePosition, lastImage, imageOffsetX, imageOffsetY);
  return result;
}

/**
 * Displays a tango image attribute.
 * @param attributeInfo The tango image attribute.
 */
export function DisplayImageAttribute(attributeInfo: AttributeInfo) {
  const { device, attribute, dataformat, format, tangoDB, onClose } = attributeInfo;

  const fullName = device + "/" + attribute;
  const [value, setValue] = useState(null);

  useEffect(() => {
    const emitter = TangoAPI.changeEventEmitter(tangoDB, [fullName]);
    return emitter((frame: any) => setValue(frame.value));
  }, [fullName, setValue, tangoDB]);
  const position = UseMousePosition();
  mousePosition = position;

  const content =
    value === undefined || value === null ? (
      "Loading..."
    ) : dataformat === "IMAGE" ? (
      <DisplayImage value={value as any} format={parseFormat(format)} />
    ) : (
      "Unupported dataformat: " + dataformat
    );

  return (
    <Modal title={fullName}>
      <Modal.Body>{content}</Modal.Body>
      <Modal.Footer>
        <button
          type="button"
          className="btn btn-outline-secondary"
          onClick={() => onClose()}
        >
          Close
        </button>
      </Modal.Footer>
    </Modal>
  );
}
