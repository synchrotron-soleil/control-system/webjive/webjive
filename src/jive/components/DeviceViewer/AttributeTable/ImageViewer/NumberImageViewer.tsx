import { Gradient, generateGradient } from "../Gradient";
import { DrawAxis } from "../Axis";
import { NumberImage } from "./ImageType";

var gradient: Gradient;

/**
 * Displays a number image.
 * @param image The image to display.
 * @param context The canvas in which to draw the image representation.
 * @param width The image width.
 * @param height The image height.
 * @param imageOffsetX The image offset in X.
 * @param imageOffsetY The image offset in Y.
 * @param gradientWidth The gradient width.
 * @param gradientOffset The gradient offset.
 * @param axisFontSize The axis font size.
 * @param axisFontFamily The axis font family.
 */
export default function DisplayNumberImage(image: NumberImage, context: CanvasRenderingContext2D, width: number, height: number, imageOffsetX: number, imageOffsetY: number, gradientWidth: number, gradientOffset: number, axisFontSize: number, axisFontFamily: string) {
  if (gradient === undefined) {
    gradient = generateGradient();
  }
  let max = -Infinity;
  let min = Infinity;
  for (let row of image) {
    for (let value of row) {
      if ((!isNaN(value)) && (value !== Infinity) && (value !== -Infinity)) {
        if (value < min) {
          min = value;
        }
        if (value > max) {
          max = value;
        }
      }
    }
  }
  if ((max === -Infinity) && (min === Infinity)) {
    max = 1;
    min = 0;
  } else if (min === max) {
    max += 0.5;
    min -= 0.5;
  }
  const imageData = context.createImageData(width, height);
  let index = 0;
  let step = (max - min) / (gradient.gradientSize - 1);
  for (let row of image) {
    for (let value of row) {
      let r = 0, g = 0, b = 0, alpha = 0;
      if ((value === Infinity) || (value === -Infinity) || isNaN(value)) {
        r = 255;
        g = 255;
        b = 255;
        alpha = 150;
      } else {
        let colorIndex = Math.round((value - min) / step);
        if (colorIndex < 0) {
          colorIndex = 0;
        }
        if (colorIndex >= gradient.gradientSize) {
          colorIndex = gradient.gradientSize - 1;
        }
        r = gradient.rgradient[colorIndex];
        g = gradient.ggradient[colorIndex];
        b = gradient.bgradient[colorIndex];
        alpha = 255;
      }
      imageData.data[index++] = r; // red
      imageData.data[index++] = g; // green
      imageData.data[index++] = b; // blue
      imageData.data[index++] = alpha; // alpha
    } // end for (let value of row)
  } // end for (let row of image)
  context.putImageData(imageData, imageOffsetX, imageOffsetY);
  const gradientData = context.createImageData(gradientWidth, height);
  index = 0;
  step = height / (gradient.gradientSize - 1);
  for (let y = 0; y < height; y++) {
    let colorIndex = Math.round(y / step);
    for (let x = 0; x < gradientWidth; x++) {
      const index = (height - (y + 1)) * gradientWidth * 4 + x * 4;
      gradientData.data[index] = gradient.rgradient[colorIndex]; // red
      gradientData.data[index + 1] = gradient.ggradient[colorIndex]; // green
      gradientData.data[index + 2] = gradient.bgradient[colorIndex]; // blue
      gradientData.data[index + 3] = 255; // alpha
    }
  }
  context.putImageData(gradientData, imageOffsetX + width + gradientOffset, imageOffsetY);
  DrawAxis(context, min, max, height, imageOffsetX + width + gradientOffset + gradientWidth, imageOffsetY, axisFontSize, axisFontFamily, false, false, true, true);
}
