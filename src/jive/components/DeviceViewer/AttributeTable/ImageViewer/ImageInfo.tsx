import { isValidArrayIndex } from "../../../../../shared/utils/ArrayUtils";
import { formatValue, NumberFormat } from "../../../../../shared/utils/NumberFormat";
import { MousePosition } from "../UseMousePosition";
import { DisplayableImage } from "./ImageType";

/**
 * Recovers the surrounding rectangle of an element identified by an id. If not found, it will return the {0, 0, 0, 0} rectangle.
 * @param id The element id.
 */
function getBoundingRect(id: string) {
  let rect: DOMRect;
  const obj = document.getElementById(id);
  if ((obj === undefined) || (obj === null)) {
    rect = new DOMRect(0, 0, 0, 0);
  } else {
    rect = obj.getBoundingClientRect();
    if ((rect === undefined) || (rect === null)) {
      rect = new DOMRect(0, 0, 0, 0);
    }
  }
  return rect;
}

function format(value: any, lastImage: DisplayableImage): string | null | undefined {
  let formattedValue: string | null | undefined;
  let nf: NumberFormat | undefined;
  if (lastImage === null || lastImage === undefined) {
    nf = undefined;
  } else {
    nf = lastImage.format;
  }
  if (value === undefined) {
    formattedValue = undefined;
  } else if (typeof (value) === 'number') {
    formattedValue = formatValue(value as number, nf);
  } else {
    formattedValue = '' + value;
  }
  return formattedValue;
}

/**
 * Updates the text of a div to indicate image width and height.
 * @param div The div.
 * @param width The image width.
 * @param height The image height.
 */
function notifyDimensions(div: HTMLElement, width: number, height: number) {
  div.textContent = 'width: ' + width + ' height: ' + height;
}

/**
 * Updates the text of a div to indicate some information of a matrix (like value at given coordinates).
 * @param div The div.
 * @param x The x coordinate.
 * @param y The y coordinate.
 * @param width The matrix width.
 * @param height The matrix height.
 * @param image The DisplayableImage that contains the matrix.
 */
function notifyInfo(div: HTMLElement, x: number, y: number, width: number, height: number, image: DisplayableImage) {
  if (isValidArrayIndex(x, width) && isValidArrayIndex(y, height)) {
    let row = image.value[y];
    if ((row !== undefined) && (row !== null) && (row.length > x)) {
      div.textContent = 'x: ' + x + '(' + width + ') y: ' + y + '(' + height + ') -> ' + format(row[x], image);
    } else {
      notifyDimensions(div, width, height);
    }
  } else {
    notifyDimensions(div, width, height);
  }
}

/**
 * Updates the information text displayed under the image.
 * @param infoDiv The id of the information <div className=""></div>
 * @param imageCanvas The id of the image canvas.
 * @param mousePosition The mouse position.
 * @param lastImage The last known image.
 * @param imageOffsetX The image offset in X.
 * @param imageOffsetY The image offset in Y.
 */
export default function updateImageInfo(infoDiv: string, imageCanvas: string, mousePosition: MousePosition, lastImage: DisplayableImage | undefined, imageOffsetX: number, imageOffsetY: number) {
  let div = document.getElementById(infoDiv);
  if ((div !== undefined) && (div !== null)) {
    let imageData = lastImage;
    if ((imageData === undefined) || (imageData === null)) {
      div.textContent = '';
    } else {
      let image = imageData.value;
      let x: number, y: number;
      if ((mousePosition === undefined) || (mousePosition === null)) {
        x = NaN;
        y = NaN;
      } else {
        x = mousePosition.x;
        y = mousePosition.y;
      }
      const rect = getBoundingRect(imageCanvas);
      const offsetX = imageOffsetX + rect.x, offsetY = imageOffsetY + rect.y;
      const height = (image === undefined) || (image === null) ? 0 : image.length;
      const width = (height === 0) || (image[0] === undefined) || (image[0] === null) ? 0 : image[0].length;
      if (width === 0) {
        div.textContent = '';
      } else if (isNaN(x) || isNaN(y)) {
        notifyDimensions(div, width, height);
      } else {
        notifyInfo(div, Math.round(x - offsetX), Math.round(y - offsetY), width, height, imageData);
      }
    }
  }
}
