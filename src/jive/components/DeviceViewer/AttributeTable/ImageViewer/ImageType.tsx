import { NumberFormat } from "../../../../../shared/utils/NumberFormat";

export type NumberImage = number[][];
export type BooleanImage = boolean[][];
export type StringImage = string[][];

export interface DisplayableImage {
  value: BooleanImage | NumberImage | StringImage;
  format: NumberFormat | undefined;
}

export interface AttributeInfo {
  tangoDB: string;
  device: string;
  attribute: string;
  dataformat: string;
  datatype: string;
  format: string;
  onClose: () => void;
}

