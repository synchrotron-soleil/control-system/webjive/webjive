import React from "react";
import { StringImage } from "./ImageType";

/**
 * Displays a string image.
 * @param image The image to display.
 * @param width The image width.
 * @param height The image height.
 */
 export default function DisplayStringImage(image: StringImage, width: number, height: number) {
  let result: JSX.Element;
  if ((height === 0) || (width === 0)) {
    result = <pre style={{ height: "15em", overflow: "scroll" }}>
      {JSON.stringify(image, null, 2)}
    </pre>;
  } else {
    let cols = new Array(width);
    for (let x = 0; x < width; x++) {
      cols[x] = x;
    }
    result = (
      <div style={{ display: "block", width: "100%", height: "300px", overflow: "scroll" }}>
        <table style={{ border: "1px solid #aaaaaa", borderCollapse: "collapse" }}>
          <tr style={{ border: "1px solid #aaaaaa", borderCollapse: "collapse" }}>
            <th style={{ border: "1px solid #aaaaaa", borderCollapse: "collapse", backgroundColor: "#eeeeee" }}> </th>
            {image[0].map((item, index) => (
              <th style={{ border: "1px solid #aaaaaa", borderCollapse: "collapse", backgroundColor: "#eeeeee" }}>{index}</th>
            ))}
          </tr>
          {image.map((item, index) => (
            <tr style={{ border: "1px solid #aaaaaa", borderCollapse: "collapse" }}>
              <th style={{ border: "1px solid #aaaaaa", borderCollapse: "collapse", backgroundColor: "#eeeeee" }}>{index}</th>
              {item.map((value) => (
                <td style={{ border: "1px solid #aaaaaa", borderCollapse: "collapse" }}>{value}</td>
              ))}
            </tr>
          ))}
        </table>
      </div >
    )
  }
  return result;
}
