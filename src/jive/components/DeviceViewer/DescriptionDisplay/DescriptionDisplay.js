import React, { useState } from "react";
import cx from "classnames";
import Tooltip from "react-tooltip-lite";

import Modal from "../../../../shared/modal/components/Modal/Modal";

import "./DescriptionDisplay.css";
import { Button } from "react-bootstrap";

function isUnset(value) {
  return ((value === undefined) || (value === null) || (value === ""));
}

function quickViewString(titles, values) {
  let str = '';
  if ((titles !== undefined) && (titles !== null) && (values !== undefined) && (values !== null) && (titles.length === values.length)) {
    for (let i = 0; i < titles.length; i++) {
      if (!isUnset(values[i])) {
        if (str === '') {
          str = str + titles[i] + values[i];
        } else {
          str = str + '\n' + titles[i] + values[i];
        }
      }
    }
  }
  return str;
}

function DescriptionDisplay({ name, min, max, format, unit, description }) {
  const [onDisplay, setOnDisplay] = useState(false);

  return (
    <>
      {onDisplay && (
        <Modal title={name}>
          <Modal.Body>
            <div style={{ whiteSpace: "pre-wrap" }}>
              <table class="description-table">
                <tr>
                  <th class="description-th">Min</th><td class="description-td">{min}</td>
                </tr>
                <tr>
                  <th class="description-th">Max</th><td class="description-td">{max}</td>
                </tr>
                <tr>
                  <th class="description-th">Format</th><td class="description-td">{format}</td>
                </tr>
                <tr>
                  <th class="description-th">Unit</th><td class="description-td">{unit}</td>
                </tr>
                <tr>
                  <th class="description-th">Description</th><td class="description-td">{description}</td>
                </tr>
              </table>
            </div>
            {/* <div style={{ whiteSpace: "pre-wrap" }}>{'min: ' + min + '\nmax: ' + max + '\ndescription: ' + description}</div> */}
          </Modal.Body>
          <Modal.Footer>
            <Button
              type="button"
              variant="primary"
              onClick={() => setOnDisplay(false)}
            >
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      )}
      <Tooltip
        content={quickViewString(['min: ', 'max: ', 'format: ', 'unit: ', 'description: '], [min, max, format, unit, description])}
        useDefaultStyles={true}
        hoverDelay={0}
        direction="left"
      >
        <i
          className={cx("DescriptionDisplay fa fa-info-circle", {
            "no-description": (description === "No description" && isUnset(min) && isUnset(max) && isUnset(format) && isUnset(unit))
          })}
          onClick={() => setOnDisplay(true)}
        />
      </Tooltip>
    </>
  );
}

export default DescriptionDisplay;
