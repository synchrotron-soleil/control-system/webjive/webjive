import React from "react";

export const StateIndicator = ({ state }) => {
  const states = {
    ON: "on",
    OFF: "off",
    CLOSE: "close",
    OPEN: "open",
    INSERT: "insert",
    EXTRACT: "extract",
    MOVING: "moving",
    STANDBY: "standby",
    FAULT: "fault",
    INIT: "init",
    RUNNING: "running",
    ALARM: "alarm",
    DISABLE: "disable",
    UNKNOWN: "unknown"
  };

  const classSuffix = states[state] || "invalid";
  return (
    <span className={`StateIndicatorLabel ${classSuffix}`} >
      {state}
    </span>
  );
};
