import React from 'react'
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import InputList from './InputList'

configure({ adapter: new Adapter() });

const tangoDB = "testdb"
const inputDefinitions = JSON.parse('{ "name": { "type": "string", "label": "Name:", "default": "Name", "placeholder": "Name of variable", "required": true }, "variable": { "type": "variable", "label": "Variable:" } }');
const inputs = JSON.parse('{ "name": "Name", "variable": "asd" }')
const widgets = JSON.parse('[ { "_id": "608c07e22708ac001305a307", "id": "2", "x": 25, "y": 3, "canvas": "0", "width": 30, "height": 7, "type": "PARAMETRIC_WIDGET", "inputs": { "name": "Name", "variable": "testVariable" }, "order": 0, "valid": true } ]')
const widgetType = "PARAMETRIC_WIDGET";
const nonEditable = false;
let variables = [];


describe("Test input list", () => {
        it("run without crash without variable using PARAMETRIC_WIDGET", () => {
            const element = React.createElement(InputList, {
            tangoDB: tangoDB,
            inputDefinitions: inputDefinitions,
            inputs: inputs,
            widgets: widgets,
            onChange: () => {},
            onAdd: () => {},
            onDelete: () => {},
            widgetType: widgetType,
            basePath: undefined,
            nonEditable: nonEditable,
            variables: variables,
        })

        const shallowElement = mount(element);
        expect(shallowElement.html()).toContain("No variables defined")
    })

    it("check if render with variables", () => {

        let variableName = "testVariable"
        variables = JSON.parse('[ { "id": "h0ajdfaa8fb9", "name": "'+variableName+'", "class": "DataBase", "device": "sys/database/2" } ]')

        const element = React.createElement(InputList, {
        tangoDB: tangoDB,
        inputDefinitions: inputDefinitions,
        inputs: inputs,
        widgets: widgets,
        onChange: () => {},
        onAdd: () => {},
        onDelete: () => {},
        widgetType: widgetType,
        basePath: undefined,
        nonEditable: nonEditable,
        variables: variables,
    })

    const shallowElement = mount(element);

    expect(shallowElement.html()).not.toContain("No variables defined")
    expect(shallowElement.find( {value: variableName} ).length).toBe(1)
    
})
})