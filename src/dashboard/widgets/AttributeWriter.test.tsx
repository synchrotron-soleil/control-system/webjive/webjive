import React from "react";

import {
  AttributeInput,
  NumberInputDefinition,
  SelectInputDefinition,
  ColorInputDefinition,
} from "../types";

import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import AttributeWriter from "./AttributeWriter";

type Input = {
  attribute: AttributeInput;
  showDevice: boolean;
  showAttribute: SelectInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
  size: NumberInputDefinition;
  font: SelectInputDefinition;
};

configure({ adapter: new Adapter() });

describe("AttributeWriter Display", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;

  it("render with DevBoolean mode run", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      dataType: "DevBoolean",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: true,
      writeValue: "",
      label: "",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: true,
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    expect(shallow(element).html()).toContain("sys/tg_test/1/:");
  });

  it("render with DevBoolean mode edit", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "boolean_scalar",
      dataType: "DevBoolean",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: true,
      writeValue: "",
      label: "label",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    expect(shallow(element).html()).toContain("example");
  });

  it("render without device mode edit", () => {
    myAttributeInput = {
      device: "",
      attribute: null,
      dataType: "",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: true,
      writeValue: "",
      label: "label",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      showAttribute: "Name",
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });
    expect(shallow(element).html()).toContain("attributeName:");
  });

  it("render with DevEnum mode run with invalid value", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevEnum",
      dataType: "DevEnum",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: "",
      writeValue: "",
      label: "label",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    let shallowElement = shallow(element);
    //assign invalid value
    shallowElement.setState({ input: "2a" });
    expect(shallowElement.html()).toContain("The input value is not numeric.");
  });

  it("render with unknow not implemented", () => {
    myAttributeInput = {
      device: "unknow",
      attribute: "unknow",
      dataType: "unknow",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: "",
      writeValue: "",
      label: "label",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      showAttribute: "Name",
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });
    expect(shallow(element).html()).toContain("unknow not implemented");
  });

  it("render DevString", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevString",
      dataType: "DevString",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: "",
      writeValue: "",
      label: "",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    let shallowElement = shallow(element);
    shallowElement.setState({ input: "text" });
    expect(shallowElement.html()).toContain("text");
  });

  it("render DevDouble", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevDouble",
      dataType: "DevDouble",
      dataFormat: "scalar",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: 1,
      writeValue: 1,
      label: "label",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    let shallowElement = mount(element);

    shallowElement.setState({ input: "0" });

    expect(shallowElement.html()).toContain('value="0"');
  });

  it("render DevEnum", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevEnum",
      dataType: "DevEnum",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: "1",
      writeValue: "1",
      label: "label",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: true,
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    let shallowElement = mount(element);

    shallowElement.setState({ input: "1as" });
    shallowElement.setState({ validation: true });

    expect(shallowElement.html()).toContain('value="1as"');
  });

  it("render DevBoolean", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevBoolean",
      dataType: "DevBoolean",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: "1",
      writeValue: "1",
      label: "label",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: true,
      font: "arial",
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    let shallowElement = mount(element);

    shallowElement.setState({ input: "1as" });
    shallowElement.setState({ validation: true });

    expect(shallowElement.html()).toContain('value="t"');
  });

  it("simulate focus", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevString",
      dataType: "DevString",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: "stringSend",
      writeValue: "stringSend",
      label: "",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    var commandArrayWidget = mount(element);
    commandArrayWidget.setState({ input: "stringSend" });
    expect(commandArrayWidget.html()).toContain('placeholder="stringSend"');
    commandArrayWidget.find("form").simulate("submit");
    expect(commandArrayWidget.html()).toContain("The input can't be empty");
    commandArrayWidget.find("input").simulate("focus");
    commandArrayWidget.find("input").simulate("blur");
    commandArrayWidget.find("input").simulate("change");
    expect(commandArrayWidget.html()).toContain('placeholder="stringSend"');
  });

  it("submit on pending", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevString",
      dataType: "DevString",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: "stringSend",
      writeValue: "stringSend",
      label: "",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    var commandArrayWidget = mount(element);
    commandArrayWidget.setState({ input: "stringSend" });
    commandArrayWidget.setState({ pending: true });
    expect(commandArrayWidget.html()).toContain('stringSend');
    commandArrayWidget.find("form").simulate("submit");
  });

  it("submit on empty string", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevString",
      dataType: "DevString",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: "stringSend",
      writeValue: "stringSend",
      label: "",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    var commandArrayWidget = mount(element);
    commandArrayWidget.setState({ input: "" });
    expect(commandArrayWidget.html()).toContain("The input can't be empty");
    commandArrayWidget.find("form").simulate("submit");
  });

  it("submit DevDouble", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevDouble",
      dataType: "DevDouble",
      dataFormat: "scalar",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: 1,
      writeValue: 1,
      label: "",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    var commandArrayWidget = mount(element);
    commandArrayWidget.setState({ input: "1" });
    expect(commandArrayWidget.html()).toContain("1.00");
    commandArrayWidget.find("form").simulate("submit");
  });

  it("submit DevBoolean", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevBoolean",
      dataType: "DevBoolean",
      dataFormat: "scalar",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: 1,
      writeValue: 1,
      label: "",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    var commandArrayWidget = mount(element);
    commandArrayWidget.setState({ input: "1" });
    expect(commandArrayWidget.html()).toContain('placeholder="1"');
    commandArrayWidget.find("form").simulate("submit");
  });

  it("submit DevBoolean  with f", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevBoolean",
      dataType: "DevBoolean",
      dataFormat: "scalar",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: 1,
      writeValue: 1,
      label: "",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    var commandArrayWidget = mount(element);
    commandArrayWidget.setState({ input: "f" });
    expect(commandArrayWidget.html()).toContain('value="f"');
    commandArrayWidget.find("form").simulate("submit");
  });

  it("submit DevDouble not accepted input", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "DevDouble",
      dataType: "DevDouble",
      dataFormat: "scalar",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: 1,
      writeValue: 1,
      label: "",
      history: [],
      timestamp: 0,
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      attribute: myAttributeInput,
      size: 1,
    };

    const element = React.createElement(AttributeWriter.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    var commandArrayWidget = mount(element);
    commandArrayWidget.setState({ input: "1as" });
    expect(commandArrayWidget.html()).toContain('1as');
    commandArrayWidget.find("form").simulate("submit");
  });
});
