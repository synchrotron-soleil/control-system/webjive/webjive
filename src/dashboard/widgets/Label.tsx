import React from "react";
import {
  WidgetDefinition,
  StringInputDefinition,
  ColorInputDefinition,
  NumberInputDefinition,
  SelectInputDefinition,
  StyleInputDefinition
} from "../types";
import { parseCss } from "../components/Inspector/StyleSelector";
import { WidgetProps } from "./types";

type Inputs = {
  text: StringInputDefinition;
  backgroundColor: ColorInputDefinition;
  textColor: ColorInputDefinition;
  linkTo: StringInputDefinition;
  size: NumberInputDefinition;
  font: SelectInputDefinition;
  borderWidth: NumberInputDefinition;
  borderColor: ColorInputDefinition;
  customCss: StyleInputDefinition;
};

type Props = WidgetProps<Inputs>;

const Inner = ({ mode, text, linkTo, size, font }) => {
  if (mode === "library") {
    return <span>Label</span>;
  }

  if (text === "" && mode === "edit") {
    return <span style={{ fontStyle: "italic" }}>Empty</span>;
  }
  const style = { fontSize: size + "em" };
  if (font) {
    style["fontFamily"] = font;
  }
  const prefix = linkTo.toLowerCase().startsWith("http") ? "" : "http://";
  const content = linkTo ? (
    <a href={prefix + linkTo} rel="noopener noreferrer">
      {text}
    </a>
  ) : (
    text
  );
  return (
    <span title={`Visit ${linkTo}`} style={style}>
      {content}
    </span>
  );
};

const Label = (props: Props) => {
  const { inputs, mode, actualWidth, actualHeight } = props;
  const {
    text,
    backgroundColor,
    textColor,
    linkTo,
    size,
    font,
    borderWidth,
    borderColor,
    customCss
  } = inputs;
  const parsedCss = parseCss(customCss).data;
  return (
    <div
      style={{
        padding: "0.5em",
        backgroundColor,
        color: textColor,
        wordBreak: "break-word",
        border: borderWidth + "em solid " + borderColor,
        height: actualHeight,
        width: mode === "library" ? "100%" : actualWidth,
        ...parsedCss
      }}
    >
      <Inner mode={mode} text={text} linkTo={linkTo} size={size} font={font} />
    </div>
  );
};

const definition: WidgetDefinition<Inputs> = {
  type: "LABEL",
  name: "Label",
  defaultHeight: 2,
  defaultWidth: 10,
  inputs: {
    text: {
      label: "Text",
      type: "string",
      default: ""
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000000"
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff"
    },
    size: {
      label: "Text size (in units)",
      type: "number",
      default: 1,
      nonNegative: true
    },
    borderWidth: {
      label: "Border width (in units)",
      type: "number",
      default: 0,
      nonNegative: true
    },
    borderColor: {
      label: "Border color",
      type: "color",
      default: "#000000"
    },
    font: {
      type: "select",
      default: "Helvetica",
      label: "Font type",
      options: [
        {
          name: "Default (Helvetica)",
          value: "Helvetica"
        },
        {
          name: "Monospaced (Courier new)",
          value: "Courier new"
        }
      ]
    },
    linkTo: {
      label: "Link to",
      type: "string",
      default: "",
      placeholder: "Optional link URL"
    },
    customCss: {
      type: "style",
      default: "",
      label: "Custom css (advanced)"
    }
  }
};

export default { definition, component: Label };
