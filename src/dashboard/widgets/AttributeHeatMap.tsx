import React, { Component } from "react";

import { WidgetProps } from "./types";
import {
  WidgetDefinition,
  BooleanInputDefinition,
  AttributeInputDefinition,
  NumberInputDefinition,
  SelectInputDefinition,
  AttributeInput
} from "../types";

// In order to avoid importing the entire plotly.js library. Note that this mutates the global PlotlyCore object.
import PlotlyCore from "plotly.js/lib/core";
import PlotlyHeatmap from "plotly.js/lib/heatmap";
import createPlotlyComponent from "react-plotly.js/factory";
PlotlyCore.register([PlotlyHeatmap]);
const Plotly = createPlotlyComponent(PlotlyCore);

const sampleData = [[30, 60, 1], [20, 1, 60], [1, 20, 30]];

type Inputs = {
  attribute: AttributeInputDefinition;
  showTitle: BooleanInputDefinition;
  showAttribute: SelectInputDefinition;
  fixedScale: BooleanInputDefinition;
  maxValue: NumberInputDefinition;
  minValue: NumberInputDefinition;
};

type Props = WidgetProps<Inputs>;

interface State {
  time?: any,
}
class AttributeHeatMap extends Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      time: null,
    };
  }

  private rendered: boolean = true;
  private interval: any = null;

  componentDidMount() {
    if (process.env.REACT_APP_REFRESHING_RATE) {
      this.interval = setInterval(
        () => this.setState({ time: new Date() }),
        parseInt(process.env.REACT_APP_REFRESHING_RATE),
      );
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  shouldComponentUpdate(_, nextState) {
    if (this.rendered) {

      if (this.interval) {
        if (this.state.time !== nextState.time) {
	  this.rendered = false;
          return true;
        }

        return false;
      }
      this.rendered = false;
      return true;
    }

    return false;
  }

  private getTitle() {
    const { mode, inputs } = this.props;
    const { showTitle, attribute, showAttribute } = inputs;

    const display = this.getDisplay(attribute,showAttribute);

    return !showTitle ? null
    : mode === "library"
    ? "device/attribute"
    : `${attribute.device || "?"}/${display || "?"}`
  };

  private getLayout() {
    const { showTitle } = this.props.inputs;
    const title = this.getTitle();

    return {
        title,
        titlefont: { size: 12 },
        font: { family: "Helvetica, Arial, sans-serif" },
        margin: {
            l: 30,
            r: 15,
            t: 15 + (showTitle ? 20 : 0),
            b: 20
        },
        autosize: true,
    };
  }

  private getData() {
    const { mode, inputs } = this.props;
    const { attribute, fixedScale, minValue, maxValue } = inputs;

    const plot = {
      z: mode === "run" ? attribute.value : sampleData,
      type: 'heatmap',
    };
    
    if (fixedScale) {
      if (minValue >= maxValue) {
        plot['zmin'] = definition.inputs.minValue.default;
        plot['zmax'] = definition.inputs.maxValue.default;
      } else {
        plot['zmin'] = minValue;
        plot['zmax'] = maxValue; 
      }
    }

    return [plot];
  }

  private getDisplay(attribute: AttributeInput,showAttribute: string): string {
    let display = "";
    if(showAttribute === "Label") {
      if(attribute.label!=="") display = attribute.label;
      else display = "attributeLabel";
    } 
    else if(showAttribute === "Name"){
      if(attribute.attribute!==null) display = attribute.attribute;
      else display = "attributeName";
    } 
    return display;
  }

  public render() {
    const { mode } = this.props;

    return (
        <Plotly
            data={this.getData()}
            layout={this.getLayout()}
            config={{ staticPlot: true }}
            responsive={true}
            style={{ width: this.props.actualWidth, height: mode === "library" ? 150 : this.props.actualHeight}}
            onAfterPlot={() => this.rendered = true}
        />
    );
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "ATTRIBUTEHEATMAP",
  name: "AttributeHeatMap",
  defaultWidth: 30,
  defaultHeight: 20,
  inputs: {
    attribute: {
      label: "",
      type: "attribute",
      dataFormat: "image",
      required: true
    },
    showTitle: {
      type: "boolean",
      label: "Show Title",
      default: true
    },
    showAttribute: {
      type: "select",
      label: "Attribute display:",
      default: "Label",
      options: [
        {
          name: "Label",
          value: "Label"
        },
        {
          name: "Name",
          value: "Name"
        }
      ]
    },
    fixedScale: {
      type: "boolean",
      label: "Fixed scale",
      default: true
    },
    maxValue: {
      label: "Max scale",
      type: "number",
      default: 30,
      nonNegative: false
    },
    minValue: {
      label: "Min scale",
      type: "number",
      default: 1,
      nonNegative: false
    },
  }
};

export default { component: AttributeHeatMap, definition };
