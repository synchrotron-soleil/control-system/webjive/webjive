import React from "react";

import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import EmbedPage from "./EmbedPage";

interface Input {
    url: string;
  }

  configure({ adapter: new Adapter() });

  describe("EmbedPageTests", () => {
    let myInput: Input;
  
    it("renders library mode true without crashing", () => {
  
      myInput = {
        url: ""
      };
  
      const element = React.createElement(EmbedPage.component, {
        mode: "library",
        t0: 1,
        actualWidth: 100,
        actualHeight: 100,
        inputs: myInput
      });
      expect(shallow(element).html()).toContain("EMBED A PAGE HERE");
    });

    it("renders edit mode true without crashing", () => {
  
        myInput = {
          url: "abc.xyz"
        };
    
        const element = React.createElement(EmbedPage.component, {
          mode: "edit",
          t0: 1,
          actualWidth: 100,
          actualHeight: 100,
          inputs: myInput
        });
        expect(shallow(element).html()).toContain("http://abc.xyz");
      });

      it("renders run mode true without crashing", () => {
  
        myInput = {
          url: "abc.xyz"
        };
    
        const element = React.createElement(EmbedPage.component, {
          mode: "run",
          t0: 1,
          actualWidth: 100,
          actualHeight: 100,
          inputs: myInput
        });
        expect(shallow(element).html()).toContain("http://abc.xyz");
      });
  });