
function computeLowLog10NoCheck(value: number) {
  return Math.floor(Math.log10(Math.abs(value)));
}

function computePow10NoCheck(value: number) {
  return Math.pow(10, value);
}

export function computeLowLog10(value: number) {
  return isNaN(value) ? value : computeLowLog10NoCheck(value);
}

export function computePow10(value: number) {
  return isNaN(value) ? value : computePow10NoCheck(value);
}

export function computeLowTen(value: number) {
  return isNaN(value) ? value : computePow10NoCheck(computeLowLog10NoCheck(value));
}
