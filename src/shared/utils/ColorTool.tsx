/**
 * Returns the hexadecimal representation, formatted on 2 characters with leading 0, of a value.
 * @param value The value.
 * @returns A string.
 */
function toHex2(value: number): string {
  return value.toString(16).padStart(2, '0');
}

/**
 * Return whether a value is not a valid color part.
 * @param colorPart The value to check.
 * @returns A boolean: True if colorPart < 0 or colorPart > 255.
 */
export function isNotColorPart(colorPart: number): boolean {
  return colorPart < 0 || colorPart > 255;
}

/**
 * Return whether a value is a valid color part.
 * @param colorPart The value to check.
 * @returns A boolean: True if colorPart >= 0 and colorPart <= 255.
 */
export function isColorPart(colorPart: number): boolean {
  return colorPart >= 0 && colorPart <= 255;
}

/**
 * Returns the the red part of an rgb color value.
 * @param rgb The rgb color value.
 * @returns A number.
 */
export function getRed(rgb: number): number {
  return (rgb >> 16) & 0xFF;
}

/**
 * Returns the the green part of an rgb color value.
 * @param rgb The rgb color value.
 * @returns A number.
 */
export function getGreen(rgb: number): number {
  return (rgb >> 8) & 0xFF;
}

/**
 * Returns the the blue part of an rgb color value.
 * @param rgb The rgb color value.
 * @returns A number.
 */
export function getBlue(rgb: number): number {
  return (rgb >> 0) & 0xFF;
}

/**
 * Returns the the alpha part of an argb color value.
 * @param argb The argb color value.
 * @returns A number.
 */
export function getAlpha(argb: number): number {
  return (argb >> 24) & 0xFF;
}

/**
 * Returns the hexadecimal representation of the red part of an rgb color value.
 * @param rgb The rgb color value.
 * @returns A string.
 */
export function getRedHex(rgb: number): string {
  return toHex2(getRed(rgb));
}

/**
 * Returns the hexadecimal representation of the green part of an rgb color value.
 * @param rgb The rgb color value.
 * @returns A string.
 */
export function getGreenHex(rgb: number): string {
  return toHex2(getGreen(rgb));
}

/**
 * Returns the hexadecimal representation of the blue part of an rgb color value.
 * @param rgb The rgb color value.
 * @returns A string.
 */
export function getBlueHex(rgb: number): string {
  return toHex2(getBlue(rgb));
}

/**
 * Returns the hexadecimal representation of the alpha part of an argb color value.
 * @param argb The argb color value.
 * @returns A string.
 */
export function getAlphaHex(argb: number): string {
  return toHex2(getAlpha(argb));
}

/**
 * Returns the number that represents the rgb/argb color value.
 * @param r The red part of the color. 0 <= r <= 255.
 * @param g The green part of the color. 0 <= g <= 255.
 * @param b The blue part of the color. 0 <= b <= 255.
 * @param a Optional: the alpha part of the color. 0 <= a <= 255.
 * @returns A number.
 * @throws {RangeError} if any argument is < 0 or > 255.
 */
export function toRGB(r: number, g: number, b: number, a: number = 255): number {
  if (isNotColorPart(r) || isNotColorPart(g) || isNotColorPart(b) || isNotColorPart(a)) {
    throw RangeError('toRGB arguments must be between 0 and 255!')
  }
  return ((a & 0xFF) << 24) | ((r & 0xFF) << 16) | ((g & 0xFF) << 8) | ((b & 0xFF) << 0);
}
