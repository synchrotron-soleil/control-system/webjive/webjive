export enum FormatType {
    Float, Scientific, Integer, Hexadecimal
}

export interface NumberFormat {
    int: number | undefined,
    decimal: number | undefined,
    formatType: FormatType
}

export function parseFormat(format: string | null | undefined): NumberFormat | undefined {
    let numberFormat: NumberFormat | undefined = undefined;
    if (format !== undefined && format !== null && format !== '' && format.charAt(0) === '%') {
        let lower = format.toLowerCase();
        let last = lower.charAt(lower.length - 1);
        let index = format.indexOf('.');
        let int: number | undefined = undefined;
        let decimal: number | undefined = undefined;
        let formatType: FormatType;
        if (last === 'x' || last === 'd') {
            if (index < 0) {
                formatType = last === 'x' ? FormatType.Hexadecimal : FormatType.Integer;
                if (format.length === 2) {
                    numberFormat = { int, decimal, formatType };
                } else {
                    try {
                        int = parseInt(format.substring(1, format.length - 1));
                        numberFormat = { int, decimal, formatType };
                    } catch (error) {
                        numberFormat = undefined;
                    }
                }
            }
        } else if (last === 'f' || last === 'e') {
            formatType = last === 'f' ? FormatType.Float : FormatType.Scientific;
            if (format.length === 2) {
                numberFormat = { int, decimal, formatType };
            } else if (index > 0) {
                try {
                    if (index > 1) {
                        int = parseInt(format.substring(1, index));
                        decimal = parseInt(format.substring(index + 1, format.length - 1));
                        int -= decimal;
                        if (int < 1) {
                            int = 1;
                        }
                        numberFormat = { int, decimal, formatType };
                    } else {
                        decimal = parseInt(format.substring(index + 1, format.length - 1));
                        numberFormat = { int, decimal, formatType };
                    }
                } catch (error) {
                    numberFormat = undefined;
                }
            } else {
                try {
                    int = parseInt(format.substring(1, format.length - 1));
                    numberFormat = { int, decimal, formatType };
                } catch (error) {
                    numberFormat = undefined;
                }
            }
        } // end else if (last === 'f' || last === 'e')
    } // end if (format != undefined && format != null && format !== '' && format.charAt(0) === '%')
    return numberFormat;
}

function addLeadingZeroes(str: string, int: number | undefined): string {
    if (int !== undefined && str.length > 0) {
        let negative = str.charAt(0) === '-';
        let positive = (!negative) && str.charAt(0) === '+';
        if (negative || positive) {
            str = str.substring(1);
        }
        if (str !== 'NaN' && str !== 'Infinity') {
            let index = str.indexOf('.');
            if (index < 0) {
                index = str.length;
            }
            while (index < int) {
                str = '0' + str;
                index++;
            }
        }
        if (negative) {
            str = '-' + str;
        } else if (positive) {
            str = '+' + str;
        }
}
    return str;
}

export function formatValue(value: number | null | undefined, format: NumberFormat | undefined): string | null | undefined {
    let formatedValue: string | null | undefined;
    if (value === undefined) {
        formatedValue = undefined;
    } else if (value === null) {
        formatedValue = null;
    } else if (format === undefined) {
        formatedValue = value.toString();
    } else {
        try {
            switch (format.formatType) {
                case FormatType.Float:
                    if (format.decimal === undefined || format.decimal < 0 || format.decimal > 20) {
                        // According to documentation, toFixed argument must be between 0 and 20
                        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toFixed
                        formatedValue = value.toString();
                        if (formatedValue.indexOf('.') < 0) {
                            formatedValue = formatedValue + '.0';
                        }
                    } else {
                        formatedValue = value.toFixed(format.decimal);
                    }
                    break;
                case FormatType.Scientific:
                    if (format.decimal === undefined || format.decimal < 0 || format.decimal > 100) {
                        // According to documentation, toExponential argument must be between 0 and 100
                        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toExponential
                        formatedValue = value.toExponential();
                    } else {
                        try {
                            formatedValue = value.toExponential(format.decimal);
                        } catch (error) {
                            formatedValue = value.toExponential();
                        }
                    }
                    break;
                case FormatType.Integer:
                    formatedValue = Math.floor(value).toString();
                    break;
                case FormatType.Hexadecimal:
                    formatedValue = Math.floor(value).toString(16);
                    break;
            }
        } catch (error) {
            formatedValue = value.toString();
        }
        formatedValue = addLeadingZeroes(formatedValue as string, format.int);
    } // end if (value == undefined) else if (value == null) else if (format == undefined) else
    return formatedValue;
}

export function format(value: number | null | undefined, format: string | null | undefined): string | null | undefined {
    return formatValue(value, parseFormat(format));
}

export function isValidFormat(format: string): boolean {
    return parseFormat(format) !== undefined;
}