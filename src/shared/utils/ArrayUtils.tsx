
/**
 * Finds the index of a value in an uniformly distributed ordered array.
 * @param value The value.
 * @param min The array minimum value.
 * @param step The array step (difference between 2 consecutive values).
 */
export function getIndexInArray(value: number, min: number, step: number) {
  return Math.round((value - min) / step);
}

/**
 * Finds the index of a value in an uniformly distributed ordered array, limiting the result between 0 and last index.
 * @param value The value.
 * @param min The array minimum value.
 * @param step The array step (difference between 2 consecutive values).
 * @param length The array length.
 */
export function getBoundedIndexInArray(value: number, min: number, step: number, length: number) {
  let index = getIndexInArray(value, min, step);
  if (length > 0 && index >= length) {
    index = length - 1;
  }
  if (index < 0) {
    index = 0;
  }
  return index;
}

/**
 * Recovers the step (difference between 2 consecutive values) of an uniformly distributed ordered array.
 * @param min The array minimum value.
 * @param max The array maximum value.
 * @param length The array length.
 */
export function recoverStep(min: number, max: number, length: number) {
  return length > 1 ? (max - min) / (length - 1) : 0;
}

/**
 * Recovers the length of an uniformly distributed ordered array.
 * @param min The array minimum value.
 * @param max The array maximum value.
 * @param step The array step (difference between 2 consecutive values).
 */
export function recoverLength(min: number, max: number, step: number) {
  let length: number;
  let tempVal = Math.abs((max - min) / step);
  let floor = Math.floor(tempVal);
  let round = Math.round(tempVal);
  // testing floating calculation error margin
  if (Math.abs(tempVal - round) < 1e-10) {
    length = round + 1;
  } else {
    length = floor + 1;
  }
  // floor and round are already limited to Long.MAX_VALUE, but previous test is not.
  if (length < 0) {
    length = 1;
  }
  return length;
}

/**
 * Returns whether a number can be considered as a valid array index.
 * @param index The number.
 * @param length The array length.
 */
export function isValidArrayIndex(index: number, length: number) {
  return (index != null) && (!isNaN(index)) && (index > -1) && (index < length);
}

/**
 * Generates a new uniform number array of given length, starting at given minimum and ending at given maximum.
 * @param length The desired array length.
 * @param min Optional: the minimum value. Default = 0.
 * @param max Optional: the maximum value. Default = length - 1.
 * @returns A number array.
 */
export function generateNumberArray(length: number, min: number = 0, max: number = length - 1): number[] {
  let array: number[] = new Array(length);
  let step = recoverStep(min, max, length);
  for (let i = 0; i < length; i++) {
    array[i] = min + i * step;
  }
  if (length > 0) {
    array[length - 1] = max;
  }
  return array;
}