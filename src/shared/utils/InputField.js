import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { command } from  "../../jive/propTypes";

export default class InputField extends Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleExecute = this.handleExecute.bind(this);
    this.state = {
      value: '',
      valid: this.props.intype === 'DevString' || this.props.intype === 'DevVoid'
    };
  }

  /**
   * This adds quotes to each element of input array
   *
   * @param {input is an array argument} input
   */
  addQuotes(input) {
    let returnVal = [];
    input.forEach(element => {
      returnVal.push(`'${element}'`);
    });
    return returnVal;
  }

  /**
   * This is used to format input data as per the accepted type argument
   *
   * @param {value to be validated} input
   * @param {Parse as per accepted data type} acceptedType
   */
  validateInputArray(input, acceptedType) {
    let list = [];
    input = input.replace(/['"]*/g, '');

      if(input.indexOf(',') !== -1)
      {
        if('[' === input[0])
          input = input.substring(1, input.length-1);

        let data = input.split(",");

        if(acceptedType.includes("String")){
          if ('DevVarLongStringArray' === acceptedType) {
            const longArr = this.validateInputArray(input.substring(0, input.indexOf(']')), 'Long');
            const stringArr = this.validateInputArray((input.substring(input.indexOf('['), input.length)+']'), 'String');

            list = `[${longArr}],[${this.addQuotes(stringArr)}]`;

          } else if ('DevVarDoubleStringArray' === acceptedType) {
            const doubleArr = this.validateInputArray(input.substring(0, input.indexOf(']')), 'Double');
            const stringArr = this.validateInputArray((input.substring(input.indexOf('['), input.length)+']'), 'String');

            list = `[${doubleArr}],[${this.addQuotes(stringArr)}]`;

          } else {
            data.forEach(element => {
              if(acceptedType.includes("Long") || acceptedType.includes("Short"))
                element = parseInt(element);
              else if(acceptedType.includes("Double") || acceptedType.includes("Float"))
                element = parseFloat(element);

              list.push(element.toString());
            })
          }
        } else if (acceptedType.includes("Char") || acceptedType.includes("Long") || acceptedType.includes("Short")){
          data.forEach(element => {
            list.push(Number.parseInt(element));
          })

        } else{
          data.forEach(element => {
            list.push(Number.parseFloat(element));
          })
        }
      } else {
        if(acceptedType.includes("String")){
          list.push(input);

        } else {
          list.push(Number.parseFloat(input));
        }
      }

    return list;
  }

  handleChange(event) {
    if(this.props.intype === 'DevBoolean' && event.target.value.length > 0){
      this.setState({ value: event.target.value, valid: true });

    }else if(event.target.value.length > 0 && this.props.intype !== 'DevString'){

      if(this.props.intype.includes('Array')){
        this.setState({ value: event.target.value, valid: true});

      } else if(this.props.intype.includes("U") && event.target.value >= 0){
        this.setState({ value: parseInt(event.target.value, 10), valid: true });

      }else if((this.props.intype.includes("Long") || this.props.intype.includes("Short")) && !this.props.intype.includes("U")){
        this.setState({ value: parseInt(event.target.value, 10), valid: true });

      }else if(!this.props.intype.includes("U")){
        this.setState({ value: parseFloat(event.target.value, 10), valid: true });
      }
    }else if(this.props.intype === 'DevString'){
      this.setState({ value: event.target.value, valid: true});
    }else{
      this.setState({value: '', valid: false });
    }
  }

  handleKeyDown(e){
    if (e.key === 'Enter') {
      this.handleExecute(e);
    }
  }

  handleExecute(event) {
    event.preventDefault();
    let param = this.state.value;

    if(this.props.intype === 'DevString'){
      param = JSON.stringify(this.state.value);

    } else if(this.props.intype === 'DevBoolean') {
      param = (this.state.value === "true") ? 1 : 0

    } else if(this.props.intype.includes('Array')) {
      param = this.validateInputArray(this.state.value, this.props.intype);
    }

    this.props.onExecute(this.props.name, param);
    this.setState({value: '' });
  }

  render() {
    const disabled = !(this.state.valid && this.props.isEnabled);
    const intype = this.props.intype;
    let inner = null;
    const buttonLabel = this.props.buttonLabel ? this.props.buttonLabel : "Execute";

    if (intype === 'DevVoid') {
      return (
        <button className="btn btn-outline-secondary" type="button" disabled={disabled} onClick={this.handleExecute}>{buttonLabel}</button>
      );
    }

    if (intype === 'DevBoolean') {
      inner = (
        <select className="custom-select" id="inputGroupSelect04" value={this.state.value} onChange={this.handleChange}>
          <option value="" defaultValue disabled hidden>Choose...</option>
          <option value="true">True</option>
          <option value="false">False</option>
        </select>
      );
    } else if (intype.toLowerCase().includes("array")){
      inner = <input className="form-control" value={this.state.value} 
      onKeyDown={this.handleKeyDown} onChange={this.handleChange} placeholder={intype} />;
    } else if (intype.includes("U")) {
      inner = <input type="number" min="0" className="form-control" value={this.state.value} 
      onKeyDown={this.handleKeyDown} onChange={this.handleChange} placeholder={intype}/>;
    } else if (intype === 'DevString') {
      inner = <input type="text" className="form-control" value={this.state.value} 
      onKeyDown={this.handleKeyDown} onChange={this.handleChange} placeholder={intype}/>;
    } else {
      inner = <input type="number" className="form-control" value={this.state.value} 
      onKeyDown={this.handleKeyDown} onChange={this.handleChange} placeholder={intype}/>;
    }

    return (
      <div className="input-group">
        {inner}
        <div className="input-group-append">
          <button className="btn btn-outline-secondary" type="button" disabled={disabled} onClick={this.handleExecute}>{buttonLabel}</button>
        </div>
      </div>
    );
  }
}

InputField.propTypes = {
  onExecute: PropTypes.func,
  commands: PropTypes.oneOfType([PropTypes.arrayOf(command), command]),
  name: PropTypes.string,
  intype: PropTypes.string,
}