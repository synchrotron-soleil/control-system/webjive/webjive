import React from "react";
import Files from "react-files";

import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import { CommandInput } from "../../../dashboard/types";
import CommandArgsFile from "./CommandArgsFile";

configure({ adapter: new Adapter() });

describe("test CommandArgsFile core component", () => {
  let myCommandInput: CommandInput;
  let generalStyle = { marginTop: "5px", marginBottom: "5px" };

  it("render widget with file content", () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "DevVarStringArray",
      output: "",
      execute: () => null,
    };

    const element = React.createElement(CommandArgsFile, {
      label: "",
      uploadBtnCss: generalStyle,
      uploadBtnLabel: "",
      sendBtnCss: generalStyle,
      sendBtnText: "",
      fullOutputStyle: generalStyle,
      output: "output",
      mode: "run",
      requireConfirmation: false,
      command: myCommandInput,
      commandName: myCommandInput.command,
      commandDevice: myCommandInput.device,
      outerDivCss: generalStyle,
    });

    const shallowElement = mount(element);

    shallowElement.state()["fileName"] = "f1.txt";
    shallowElement.state()["fileContent"] = "[1,2,3,4]";
    shallowElement.setState({ fileType: "text" });
    shallowElement.state()["updateFileContent"] = "[1,2,3,4]";

    shallowElement.find(".file-name-wrapper").simulate("click");
    expect(shallowElement.state()["showFileViewModal"]).toBe(true);

    //close button of popup modal
    shallowElement.find(".btn-close-modal").simulate("click");
    expect(shallowElement.state()["showFileViewModal"]).toBe(false);

    //execute already executing command
    shallowElement.state()["pending"] = true;
    shallowElement.find(".btn-send").simulate("click");

    //execute command & check status
    shallowElement.state()["pending"] = false;
    shallowElement.state()["requireConfirmation"] = false;
    shallowElement.state()["fileContent"] = "[1,2,3,4]";
    shallowElement.find(".btn-send").simulate("click");
    expect(shallowElement.state()["commandStatus"]).toContain("Executed");

    const elemNoWhiteSpace = shallowElement.html().replace(/\s/g, "");
    expect(elemNoWhiteSpace).toContain("[1,2,3,4]");
    expect(elemNoWhiteSpace).toContain("f1.txt");
  });

  it("edit the content file", () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "configureArray",
      output: "sample output",
      execute: () => null,
    };

    const element = React.createElement(CommandArgsFile, {
      label: "",
      uploadBtnCss: generalStyle,
      uploadBtnLabel: "",
      sendBtnCss: generalStyle,
      sendBtnText: "",
      fullOutputStyle: generalStyle,
      output: "output",
      mode: "run",
      requireConfirmation: false,
      command: myCommandInput,
      commandName: myCommandInput.command,
      commandDevice: myCommandInput.device,
      outerDivCss: generalStyle,
    });

    const wrapper = shallow(element);

    //upload the file
    wrapper.setState({ fileName: "text.txt" });
    //use the same value
    wrapper.setState({ updateFileContent: "[1, 2, 3, 4]" });
    wrapper.setState({ fileContent: "[1, 2, 3, 4]" });
    wrapper.setState({ fileType: "text" });
    let buttonCompare = wrapper.find(".btn-compare");
    let buttonRestore = wrapper.find(".btn-restore");

    //check if buttons compare and restore are disabled
    expect(buttonCompare.html()).toContain("disable");
    expect(buttonRestore.html()).toContain("disable");

    //edit the value
    wrapper.setState({ updateFileContent: "[1, 2, 3, 4, 5]" });

    buttonCompare = wrapper.find(".btn-compare");
    buttonRestore = wrapper.find(".btn-restore");

    expect(buttonCompare.html()).not.toContain("disable");
    expect(buttonRestore.html()).not.toContain("disable");

    //restore the value
    //check if the values are still different
    expect(wrapper.state()["updateFileContent"]).not.toEqual(
      wrapper.state()["fileContent"]
    );
    buttonRestore.simulate("click");
    let buttonYes = wrapper.find(".btn-yes");
    buttonYes.simulate("click");
    expect(wrapper.state()["updateFileContent"]).toEqual(
      wrapper.state()["fileContent"]
    );

    //check Restore and Compare, Edit buttons
    //open modal

    wrapper.find(".file-name-wrapper").simulate("click");
    expect(wrapper.html()).toContain("textarea");
    buttonCompare = wrapper.find(".btn-compare");
    buttonCompare.simulate("click");
    expect(wrapper.html()).not.toContain("textarea");
    let buttonEdit = wrapper.find(".btn-edit");
    buttonEdit.simulate("click");
    expect(wrapper.html()).toContain("textarea");
  });

  it("load a binary file", () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "configureArray",
      output: "sample output",
      execute: () => null,
    };

    const element = React.createElement(CommandArgsFile, {
      label: "",
      uploadBtnCss: generalStyle,
      uploadBtnLabel: "",
      sendBtnCss: generalStyle,
      sendBtnText: "",
      fullOutputStyle: generalStyle,
      output: "output",
      mode: "run",
      requireConfirmation: false,
      command: myCommandInput,
      commandName: myCommandInput.command,
      commandDevice: myCommandInput.device,
      outerDivCss: generalStyle,
    });

    const wrapper = shallow(element);

    //upload the file
    wrapper.setState({ fileName: "text.jpg" });
    //use the same value
    wrapper.setState({ updateFileContent: "010101000101010101" });
    wrapper.setState({ fileContent: "010101000101010101" });
    wrapper.setState({ fileType: "binary" });

    //check if it show a message that said that it is a binary file and hides the textarea
    //open modal
    wrapper.find(".file-name-wrapper").simulate("click");
    expect(wrapper.html()).toContain(
      "Unable to generate a preview for a binary file"
    );
    expect(wrapper.html()).not.toContain("textarea");
  });

  it("Send command from Dashboard View", () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "configureArray",
      output: "sample output",
      execute: () => jest.fn(),
    };

    const element = React.createElement(CommandArgsFile, {
      label: "",
      uploadBtnCss: generalStyle,
      uploadBtnLabel: "",
      sendBtnCss: generalStyle,
      sendBtnText: "",
      fullOutputStyle: generalStyle,
      output: "output",
      mode: "run",
      requireConfirmation: false,
      command: myCommandInput,
      commandName: myCommandInput.command,
      commandDevice: myCommandInput.device,
      outerDivCss: generalStyle,
    });

    const wrapper = shallow(element);

    //upload the file
    wrapper.setState({ fileName: "text.jpg" });
    //use the same value
    wrapper.setState({ updateFileContent: "010101000101010101" });
    wrapper.setState({ fileContent: "010101000101010101" });
    wrapper.setState({ fileType: "binary" });

    //execute command from dashboard
    expect(wrapper.state()["commandStatus"]).toContain("");
    wrapper.find(".btn-send").simulate("click");
    expect(wrapper.state()["commandStatus"]).toContain("Executed");
  });

  it("Send command from Devices view", () => {
    const element = React.createElement(CommandArgsFile, {
      label: "",
      uploadBtnCss: generalStyle,
      uploadBtnLabel: "",
      sendBtnCss: generalStyle,
      sendBtnText: "",
      fullOutputStyle: generalStyle,
      output: "output",
      mode: "run",
      requireConfirmation: false,
      commandFromDevice: () => jest.fn(),
      commandName: "command_from_devices",
      commandDevice: "device_from_devices",
      outerDivCss: generalStyle,
    });

    let wrapper = shallow(element);

    //upload the file
    wrapper.setState({ fileName: "text.jpg" });
    //use the same value
    wrapper.setState({ updateFileContent: "010101000101010101" });
    wrapper.setState({ fileContent: "010101000101010101" });
    wrapper.setState({ fileType: "binary" });

    //execute command from Devices
    wrapper.find(".btn-send").simulate("click");
    expect(wrapper.state()["commandStatus"]).toContain("Executed");
  });

  it("test load file", async () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "configureArray",
      output: "sample output",
      execute: () => null,
    };

    const element = React.createElement(CommandArgsFile, {
      label: "",
      uploadBtnCss: generalStyle,
      uploadBtnLabel: "",
      sendBtnCss: generalStyle,
      sendBtnText: "",
      fullOutputStyle: generalStyle,
      output: "output",
      mode: "run",
      requireConfirmation: false,
      command: myCommandInput,
      commandName: myCommandInput.command,
      commandDevice: myCommandInput.device,
      outerDivCss: generalStyle,
    });

    const wrapper = mount(element);

    var blob = new Blob(["1, 2, 3, 4"], { type: "plain/text" });

    var file = new File([blob], "values.txt", {
      type: "text/plain",
      lastModified: Date.now(),
    });

    //check if it reset compare and restore values
    expect(wrapper.state()["showCompare"]).toBe(false);
    expect(wrapper.state()["restoreValue"]).toBe(false);

    let fileUploadComponent: Files = wrapper.find(Files).first();
    await fileUploadComponent.props().onChange([file]);
    expect(wrapper.state()["fileName"]).toBe(file.name);

    //check if it reset compare and restore values
    expect(wrapper.state()["showCompare"]).toBe(false);
    expect(wrapper.state()["restoreValue"]).toBe(false);
  });
});
